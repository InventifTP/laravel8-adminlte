<?php

use App\Http\Controllers\ShowClients;
use App\Http\Livewire\ShowPosts;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClientController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard.index');
})->name('dash');

Route::middleware(['auth:sanctum', 'verified'])->get('/dash', ShowPosts::class)->name('dashboard');


//-------------------------START CLIENT SECTION--------------------------

Route::get('/clients', [ShowClients::class, '__invoke'])->name('clients')->middleware(['auth']);

//--------------------------END CLIENT SECTION---------------------------


//-------------------------START EXAMPLE SECTION--------------------------

Route::get('/exampletable', function () {
    return view('main-system.pages.exampleTable');
});

//--------------------------END EXAMPLE SECTION---------------------------