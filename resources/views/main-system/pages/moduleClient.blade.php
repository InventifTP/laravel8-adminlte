@extends('adminlte::page')

@section('title', 'Clientes')

@section('content_header')
<h1>Clientes</h1>
@stop
@section('plugins.Datatables', true)
@section('content')

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Clientes Registrados</h3>
                    </div>
                    <div class="card-body">
                        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-sm-12 col-md-6"></div>
                                <div class="col-sm-12 col-md-6"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example" class="display nowrap" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Nombre</th>
                                                <th>Teléfono</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($users as $user)
                                            <tr id="{{ $user->id}}" data-object='{"key": "{{ $user->id}}_dataobj"}'>
                                                <td data-value="{{ $user->id}}">{{ $user->id}}</td>
                                                <td data-text="{{ $user->name}}">{{ $user->name}}</td>
                                                <td data-i18n="{{ $user->phone }}">{{ $user->phone }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>ID</th>
                                                <th>Nombre</th>
                                                <th>Teléfono</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="card">
    <div class="card-header">
        <h3 class="card-title">Registro de clientes</h3>
        <div class="card-tools">
            <!-- Buttons, labels, and many other things can be placed here! -->
            <!-- Here is a label for example -->
            <span class="badge badge-primary">Clientes registrados xxx</span>
        </div>
        <!-- /.card-tools -->
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        {{-- With label, invalid feedback disabled and form group class --}}
        <div class="row">
            <x-adminlte-input name="name" label="Nombre" placeholder="" fgroup-class="col-md-4"
                disable-feedback />
                
                <x-adminlte-input name="lastname" label="Apellido Paterno" placeholder="" fgroup-class="col-md-4"
                disable-feedback />

                <x-adminlte-input name="second_lastname" label="Apellido Materno" placeholder="" fgroup-class="col-md-4"
                disable-feedback />

                <x-adminlte-input name="phone" label="Teléfono" placeholder="" fgroup-class="col-md-4"
                disable-feedback />

                <x-adminlte-input name="email" label="Correo electrónico." placeholder="" fgroup-class="col-md-4"
                disable-feedback />
                
                <x-adminlte-input name="rfc" label="RFC" placeholder="" fgroup-class="col-md-4"
                disable-feedback />

                
        </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        Registro de clientes
    </div>
    <!-- /.card-footer -->
</div>
<!-- /.card -->


@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    console.log('Hi!');
    $(document).ready(function () {
        $('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    });
</script>
@stop