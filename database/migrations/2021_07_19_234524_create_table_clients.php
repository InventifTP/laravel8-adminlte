<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 60);
            $table->string('lastname', 60);
            $table->string('second_lastname', 60)->nullable();
            $table->string('identification_number', 60)->nullable();
            $table->string('address', 100)->nullable();
            $table->string('phone', 25)->index()->nullable();
            $table->string('contact_phone', 25)->index()->nullable();
            $table->string('rfc', 50)->nullable();
            $table->string('email', 60)->unique()->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        DB::table('clients')->insert([
            "name"                      =>  "Pedro",
            "lastname"                  =>  "Flintstones",
            "second_lastname"           =>  "Pica Piedra",
            "identification_number"     =>  "0000",
            "address"                   =>  "Av Siempre Viva",
            "phone"                     => "686123654",
            "contact_phone"             =>  "644885544",
            "rfc"                       =>  "CHAR881231P30",
            "email"                     => "pedro@inventif.xyz"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
