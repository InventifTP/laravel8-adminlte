<?php

namespace App\Http\Controllers;
use  App\Models\Client;

use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = [
            'code' => 200,
            'status' => 'success',
            'services' => Client::select('id','name')->get()
            ];
    
            return response()->json($result, $result['code']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return "hola";
        $rules = [
            'rfc' => 'required|unique:clients,rfc'
        ];
        $messages = [
            "rfc.unique" => "Ya hay un cliente registrado con este RFC: " . $request->rfc . ", por favor seleccione otro RFC"
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
        {
            return redirect()
                    ->route('client.create')
                    ->withInput($request->input())
                    ->withErrors($validator->errors());
        }
        else
        {
            
            $client = new Client;
            $routine->name = $request->routineName;
            $routine->description = $request->routineDescription;
            $routine->start_date = $request->routineStartDate;
            $routine->exercises = $request->exercisesID;
            $routine->prepare = $request->routinePrepareTime;
            $routine->work = $request->routineWorkTime;
            $routine->rest_exercise = $request->routineRestBetweenExerciseTime;
            $routine->cycles = $request->routineCycles;
            $routine->rounds = $request->routineRounds;
            $routine->rest_rounds = $request->routineRestBetweenRoundsTime;
            $routine->save();

            return redirect()
                    ->route('client.create')
                    ->withSuccess(["El cliente fue agregado correctamente", "Nombre: " . $request->clientName , "Apellido: " . $request->clientLastName]); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
