<?php

namespace App\Http\Livewire;

use Livewire\Component;

class AdminLiveWire extends Component
{
    public function render()
    {
        return view('livewire.admin-live-wire');
    }
}
