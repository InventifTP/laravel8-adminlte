<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use HasFactory;
    protected $table = "clients";
    use SoftDeletes;
    protected $fillable = [
        'name',
        'lastname',
        'second_lastname',
        'identification_number',
        'address',
        'phone',
        'contact_phone',
        'rfc'
    ];
}
