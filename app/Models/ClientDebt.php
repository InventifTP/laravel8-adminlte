<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientDebt extends Model
{
    use HasFactory;

    protected $table = "client_debts";
    protected $fillable = [
        'client_id',
        'amount',
        'date',
        'paid'
    ];
}
